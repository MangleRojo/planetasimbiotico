# Planeta Simbiótico

<img src="https://manglerojo.org/planetasimbiotico/images/ps_title.png">


Una aventura de imaginación científica 🌈🧪 para crear y compartir extraños y sorprendentes relatos de la vida multiespecie inspirados en el libro Planeta Simbiótico de Lynn Margulis.

Esta es una aventura hecha en casa desde la apuesta metodológica de 'crear para aprender y aprender para crear' donde niñas, niños, padres y madres exploran la simbiosis y su papel en la evolución de la vida en la Tierra.

El set de juego de Planeta Simbiótico es parte de los resultados del Laboratorio Virtual Nexo-Plural, proyecto ganador de la Beca Plataforma Bogotá en Arte, Ciencia y Tecnología 2020

¿Cómo se juega?
Planeta Simbiótico es una experiencia híbrida. Se juega con un tablero y teselas que se pueden imprimir, dibujar o modificar fácilmente y que se complementan con la aplicación web, el canal en Telegram y un bot conversacional.

En esta aventura los jugadores-exploradores entrelazan sus caminos en forma de micorrizas por medio de las cuales transitan los conceptos asociados con la evolución simbiótica.

Más información en:
https://manglerojo.org/planetasimbiotico